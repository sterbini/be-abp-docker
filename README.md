# Introduction

We would like to explore the potential of docker in our community.
This repoitory is there for a first attempt.

## Main use case
I have a Mac laptop and I would like to work locally on my Mac in a UNIX system. 
Some of the reasons are
- work directly on lxplus can be slow (dependent on the node). The advantage of lxplus is that it mounts `afs`, `eos`, `cvmfs` and the HTCondor interface.  
- work on SWAN can be slow and it does not mount `afs` - this can be problematic for the MAD-X mask.
- python C-compiled package can have difficulty to be maintained also for Mac/Win, e.g., `cpymad` does not work properly on Mac 
(I can install it on Mad and run small lattice, but when I try the larger ones, namely LHC, it fails).
I started to use a simple docker as a workaround (the same I used for CAS/JUAS courses), typically I use `jupiter lab` and my experience in term of performance and stability is very positive.
- I would like to mount `eos` and `afs` without suffering to much, and I would like that the possibility to share my setup not only via a set of wikis but also with press-one-button solution. With this spirit, one could prepare and share several `env`, e.g,. to track particles, to access NXCALS,...

For the moment, I am considering that you have already installed in your Mac (local host) `afs` - it is quite easy to do so, I tested only on Catalina.
You do not need to install `eos` on your Mac. This is installed inside the container (thanks to the help of Fabio Luchetti).  

# To run the docker

To lauch the docker sterbini/test

```bash
docker run -ti --cap-add SYS_ADMIN --device /dev/fuse gitlab-registry.cern.ch/sterbini/be-abp-docker:$TAG
```

where you can find the $TAG you want at https://gitlab.cern.ch/sterbini/be-abp-docker/container_registry/9245. Probably you want the latest tag. 

**At the time of the writing (2021.03.24) the latest tag was 9ac88918.**

In case you want to mount a local folder, this has to be **shared** with Docker Docker (I did it with Docker Desktop on my Mac)
and then

```bash
docker run -v /afs:/afs -ti --cap-add SYS_ADMIN --device /dev/fuse gitlab-registry.cern.ch/sterbini/be-abp-docker:$TAG
```

When you are in the docker you have to do:

``` bash
kinit $YOUR_NICE_LOGIN_NAME
```

# To make the docker

To build the docker

```bash
docker build . -t $IMAGE_NAME
```

where $IMAGE_NAME can be for example `sterbini/test`

## To export the X-graphics on Mac
From https://medium.com/@mreichelt/how-to-show-x11-windows-within-docker-on-mac-50759f4b65cb

- Install the latest XQuartz X11 server and run it
- Activate the option ‘Allow connections from network clients’ in XQuartz settings
- Quit & restart XQuartz (to activate the setting)

On the local host (Mac) do 

```bash
xhost + 127.0.0.1
```

And when you run the docker by adding 

```bash
-e DISPLAY=host.docker.internal:0
```

to the `docker run` that is

```bash
docker run --rm -e DISPLAY=host.docker.internal:0 -p 8889:8888 -ti --cap-add SYS_ADMIN --device /dev/fuse -e GRANT_SUDO=yes gitlab-registry.cern.ch/sterbini/be-abp-docker:$TAG
```

then you can test from the container
```bash
xclock
```

## Some simple operations with Docker

To see all running containers you can do (https://phoenixnap.com/kb/how-to-ssh-into-docker-container)

```bash
docker ps
```

then you can attach from another local-host terminal to the container

```
docker attach $CONTAINER_ID
```

the $CONTAINER_ID is the list of the docker processes - you can also use the $CONTAINER_NAME to attach it.

If you use `docker attach` you are attaching the very same terminal.
If you want to open a new terminal in the container you can do
```bash
docker exec -it $CONTAINER_ID /bin/bash
```

## Containers renaming, stopping and reattaching.
You can use '-n' option to name a container (no other containers with that name should existin in the Docker Host).
Then
```bash
docker run --name happy_newton -e DISPLAY=host.docker.internal:0 -p 8889:8888 -p 8822:22 -ti --cap-add SYS_ADMIN --device /dev/fuse -e GRANT_SUDO=yes -v /afs:/afs $MY_IMAGE:$MY_TAG
```

One you exit the container, it will not be removed but it will stopped. You can remove it automatically if you add `--rm` option
```
docker run --rm --name happy_newton -e DISPLAY=host.docker.internal:0 -p 8889:8888 -p 8822:22 -ti --cap-add SYS_ADMIN --device /dev/fuse -e GRANT_SUDO=yes -v /afs:/afs $MY_IMAGE:$MY_TAG
```

It you do not remove the container when you exit it you can restart it and reattach it. For example:
```
docker container start happy_newton
docker attach happy_newton
```

You can also remove a  
```
container rm happy_newton
``` 


# To be done

```
docker run --rm -e DISPLAY=host.docker.internal:0 -p 8889:8888 -ti --cap-add SYS_ADMIN --device /dev/fuse -e GRANT_SUDO=yes gitlab-registry.cern.ch/sterbini/be-abp-docker:$TAG
```

